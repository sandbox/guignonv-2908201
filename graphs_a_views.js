/**
 * @file
 * Graphs Javascript library.
 */

(function ($) {

Drupal.behaviors.graphs_a_views = {
  attach: function (context, settings) { },

  detach: function (context, settings) { },

  act: function (action_settings, caller, data) {
    switch (caller) {
      case 'getGraph':
          data['nodes'].forEach(function (node) {
            if (node.action && node.action[action_settings.id]) {
              if (!node.action[action_settings.id].processed) {
                //+...
              }
            }
          });
        break;

      default: break;
    }
  },

};

}(jQuery));
